��          �      �       0  '   1  $   Y     ~     �  #   �  a   �          9     ?  $   Q  #   v     �  �  �  H   x  x   �     :     J  F   d  �   �  E   8     ~  $   �  F   �  B   �     :               	                             
                Time before screen blanks (Minutes)     Turn on/off screen blank timeout    minutes or   seconds Could not run set-screen-blank -set Could not run turn off screen blank via xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose Could not turn on screen blank Error Screen Blank Time Screen blank time has been set to: 
 Screen blanking has been turned off Success Project-Id-Version: set-screen-blank ver. 2.2
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-15 17:56+0200
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2021
Language-Team: Greek (https://www.transifex.com/antix-linux-community-contributions/teams/120110/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
   Χρόνος πριν από την κενή οθόνη (λεπτά)     Ενεργοποίηση/απενεργοποίηση κενού χρονικού ορίου λήξης οθόνης    λεπτά ή   δευτερόλεπτα Δεν ήταν δυνατή η εκτέλεση set-screen-blank -set Δεν ήταν δυνατή η απενεργοποίηση screen blank μέσω xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose Δεν ήταν δυνατή η ενεργοποίηση screen blank Σφάλμα Κενός χρόνος οθόνης Ο χρόνος κενής οθόνης έχει οριστεί σε:
 Το κενό οθόνης έχει απενεργοποιηθεί Επιτυχία 