��          �      �       0  '   1  $   Y     ~     �  #   �  a   �          9     ?  $   Q  #   v     �  S  �  :   �  $   1     V     e  A   |  r   �  2   1     d     k  ?   �  (   �     �               	                             
                Time before screen blanks (Minutes)     Turn on/off screen blank timeout    minutes or   seconds Could not run set-screen-blank -set Could not run turn off screen blank via xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose Could not turn on screen blank Error Screen Blank Time Screen blank time has been set to: 
 Screen blanking has been turned off Success Project-Id-Version: set-screen-blank (VERSION ?)
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-14 13:09+0200
Language-Team: antiX community translations <transifex.com>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.4.2
  Zeitdauer bis zum Abschalten des Bildschirms in Minuten     Bildschirmschoner (de-)aktivieren   Minuten bzw.   Sekunden eingestellt. Konfiguration des Bildschirmschoners kann nicht gestartet werden. Bildschirmschoner konnte nicht mittels xset 0 0 0 s 0 0, xset -dpms, xset s off, xset s noexpose gestartet werden. Der Bildschirmschoner kann nicht aktiviert werden. Fehler Bildschirmschonerzeitkonstante Die Zeitkonstante bis zum Abschalten des Bildschirms wurde auf
 Der Bildschirmschoner wurde deaktiviert. Anpassung ist erfolgt 